﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hog4Controler.Interfaces
{
    public interface IMidiDriver
    {
        Int32 ID
        {
            get;
        }

        string Name
        {
            get;
        }
    }
}
