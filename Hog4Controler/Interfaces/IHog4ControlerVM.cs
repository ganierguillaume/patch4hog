﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Hog4Controler.Interfaces
{
    public interface IHog4ControlerVM
    {
        List<IMidiDriver> MidiDrivers { get; }
        ICommand TestMacro { get; }
        ICommand Start { get; }
        ICommand Stop { get; }
        ICommand ClearAllData { get; }
        ICommand ImportExcel { get; }
    }
}
