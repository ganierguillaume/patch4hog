﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hog4Controler.Interfaces;
using Sanford.Multimedia.Midi;

namespace Hog4Controler.ViewModel
{
    public class MidiDriver : IMidiDriver
    {
        public MidiDriver(int id, MidiOutCaps caps)
        {
            ID = id;
            Name = caps.name;
        }

        public int ID
        {
            get;
            private set;
        }

        public string Name
        {
            get;
            private set;
        }
    }
}
