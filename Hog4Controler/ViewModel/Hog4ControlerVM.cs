﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Hog4Controler.Interfaces;
using Hog4Controler.ViewModel;
using Sanford.Multimedia.Midi;
using System.Threading;
using System.Collections.ObjectModel;
using System.IO;
using Excel;
using System.Data;
using Microsoft.Office.Interop.Excel;

namespace Hog4Controler
{
    public class Command
    {
        public int Fixture { get; set; }
        public int DP { get; set; }
        public int Universum { get; set; }
        public int DMX { get; set; }
    }

    public class Hog4ControlerVM : ViewModelBase, IHog4ControlerVM
    {
        public int _channel;
        public int Channel
        {
            get { return _channel; }
            set { Set("Channel", ref _channel, value); }
        }

        public int _macroChannel = 1;
        public int MacroChannel
        {
            get { return _macroChannel; }
            set { Set("MacroChannel", ref _macroChannel, value); }
        }

        public int _latency = 70;
        public int Latency
        {
            get { return _latency; }
            set { Set("Latency", ref _latency, value); }
        } 

        public bool _isMacroActivated = false;
        public bool IsMacroActivated
        {
            get { return _isMacroActivated; }
            set 
            {
                Set("IsMacroActivated", ref _isMacroActivated, value);
                (this.TestMacro as RelayCommand).RaiseCanExecuteChanged();
            }
        }


        public ObservableCollection<Command> CommandCollection
        {
            get;
            set;
        }

        public enum DeviceType
        { 
            NullReference,  //	4	A null version of the reference rasterizer.
            Software,       //	3	A pluggable software device.
            Reference,      //	2	Microsoft Direct3D features are implemented in software; however, the reference rasterizer uses special CPU instructions whenever possible.
            Hardware        //	1	Hardware rasterization. Shading is done with software, hardware, or mixed transform and lighting.
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MIDIOUTCAPS
        {
            public ushort wMid;
            public ushort wPid;
            public uint vDriverVersion;     //MMVERSION
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string szPname;
            public ushort wTechnology;
            public ushort wVoices;
            public ushort wNotes;
            public ushort wChannelMask;
            public uint dwSupport;
        }

        public delegate void CB(int hdl, int msg, int inst, int p1, int p2); // callback

        #region Midi Header Structure
        [StructLayout(LayoutKind.Sequential)]
        internal struct MidiHdr
        {
            #region MidiHeader Members

            /// <summary>
            /// Pointer to MIDI data.
            /// </summary>
            public IntPtr data;

            /// <summary>
            /// Size of the buffer.
            /// </summary>
            public int bufferLength;

            /// <summary>
            /// Actual amount of data in the buffer. This value should be less than 
            /// or equal to the value given in the dwBufferLength member.
            /// </summary>
            public int bytesRecorded;

            /// <summary>
            /// Custom user data.
            /// </summary>
            public int user;

            /// <summary>
            /// Flags giving information about the buffer.
            /// </summary>
            public int flags;

            /// <summary>
            /// Reserved; do not use.
            /// </summary>
            public IntPtr next;

            /// <summary>
            /// Reserved; do not use.
            /// </summary>
            public int reserved;

            /// <summary>
            /// Offset into the buffer when aSysExByteArray callback is performed. (This 
            /// callback is generated because the MEVT_F_CALLBACK flag is 
            /// set in the dwEvent member of the MidiEventArgs structure.) 
            /// This offset enables an application to determine which 
            /// event caused the callback. 
            /// </summary>
            public int offset;

            /// <summary>
            /// Reserved; do not use.
            /// </summary>
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public int[] reservedArray;

            #endregion
        }
        #endregion

        [DllImport("winmm.dll", EntryPoint = "midiOutGetDevCaps")]
        public static extern int midiOutGetDevCaps(int uDeviceID, ref MIDIOUTCAPS lpCaps, int uSize);

        [DllImport("winmm.dll")]
        static extern UInt32 midiOutGetNumDevs();

        [DllImport("winmm.dll")]
        public static extern int midiOutPrepareHeader(IntPtr handle, IntPtr headerPtr, int sizeOfMidiHeader);
        [DllImport("winmm.dll")]
        public static extern int midiOutUnprepareHeader(IntPtr handle, IntPtr headerPtr, int sizeOfMidiHeader);
        [DllImport("winmm.dll")]
        protected static extern int midiOutOpen(out IntPtr handle, int deviceID, MidiOutProc proc, IntPtr instance, int flags);
        [DllImport("winmm.dll")]
        public static extern int midiOutGetErrorText(int errCode, StringBuilder message, int sizeOfMessage);
        [DllImport("winmm.dll")]
        public static extern int midiOutClose(IntPtr handle);
        [DllImport("winmm.dll")]
        public static extern int midiStreamClose(IntPtr handle);
        [DllImport("winmm.dll")]
        public static extern int midiStreamOpen(out IntPtr handle, ref int deviceID, int reserved, CB proc, IntPtr instance, uint flag);
        [DllImport("winmm.dll")]
        public static extern int midiStreamOut(IntPtr handle, IntPtr headerPtr, int sizeOfMidiHeader);
        [DllImport("winmm.dll")]
        public static extern int midiOutLongMsg(IntPtr handle, IntPtr headerPtr, int sizeOfMidiHeader);

        protected delegate void MidiOutProc(IntPtr handle, int msg, IntPtr instance, IntPtr param1, IntPtr param2);

        private void SendMidi(int channel, int key)
        {
            if (SelectedMidiDriver == null)
                return;

            using (OutputDevice outDevice = new OutputDevice(SelectedMidiDriver.ID))
            {
                ChannelMessageBuilder builder = new ChannelMessageBuilder();
                builder.Command = ChannelCommand.NoteOn;
                builder.MidiChannel = channel;
                builder.Data1 = key;
                builder.Data2 = 127;
                builder.Build();

                outDevice.Send(builder.Result);

                Thread.Sleep(Latency);

                builder.Command = ChannelCommand.NoteOff;
                builder.Data2 = 0;
                builder.Build();

                outDevice.Send(builder.Result);

                Thread.Sleep(Latency);
            }
        }

        public Hog4ControlerVM()
        {
            CommandCollection = new ObservableCollection<Command>();
            //CommandCollection.Add(new Command() { Fixture = 1, DP = 1, Universum = 1, DMX = 99 });

            Start = new RelayCommand(() =>
            {
                foreach (var toto in this.CommandCollection)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(toto.Fixture);
                    sb.Append("@");
                    sb.Append(toto.DP);
                    sb.Append("/");
                    sb.Append(toto.Universum);
                    sb.Append("/");
                    sb.Append(toto.DMX);

                    string toProccess = sb.ToString();
                    foreach (var letter in toProccess.ToArray())
                    {
                        switch (letter)
                        {
                            case '@':
                                {
                                    SendMidi(this.IsMacroActivated ? this.MacroChannel : this.Channel, 17);
                                    break;
                                }
                            case '/':
                                {
                                    SendMidi(this.Channel, 12);
                                    break;
                                }
                            default:
                                {
                                    SendMidi(this.Channel, int.Parse(letter.ToString()));
                                    break;
                                }
                        }
                    }

                    // send the command ENTER
                    SendMidi(this.Channel, 18);

                }
            }, () => 
            {
                return SelectedMidiDriver != null; 
            });

            Stop = new RelayCommand(() =>
            {
            }, () =>
            {
                return false;
            });

            ClearAllData = new RelayCommand(() =>
            {
                this.CommandCollection.Clear();
            }, () =>
            {
                return true;
            });

            ImportExcel = new RelayCommand(() =>
            {
                var fileDialog = new System.Windows.Forms.OpenFileDialog();
                var result = fileDialog.ShowDialog();
                switch (result)
                {
                    case System.Windows.Forms.DialogResult.OK:
                        {
                            CommandCollection.Clear();

                            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
                            Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Open(fileDialog.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                    Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                    Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                                    Type.Missing, Type.Missing);

                            Worksheet sheet = (Worksheet)wb.Sheets["patch"];

                            Range excelRange = sheet.UsedRange;

                            foreach (Microsoft.Office.Interop.Excel.Range row in excelRange.Rows)
                            {
                                if (row.Row == 1)
                                    continue;

                                Microsoft.Office.Interop.Excel.Range workingRangeCells = sheet.get_Range("A" + row.Row + ":D" + row.Row + "", Type.Missing);

                                if (workingRangeCells != null)
                                {
                                    var test = workingRangeCells.Value2;
                                    CommandCollection.Add(new Command() { Fixture = Convert.ToInt32(test[1, 1]),
                                                                          DP = Convert.ToInt32(test[1, 2]), 
                                                                          Universum = Convert.ToInt32(test[1, 3]), 
                                                                          DMX = Convert.ToInt32(test[1, 4]) });
                                }

                            }

                            break;
                        }
                    case System.Windows.Forms.DialogResult.Cancel:
                    default:
                        //TxtFile.Text = null;
                        //TxtFile.ToolTip = null;
                        break;
                }
            }, () =>
            {
                return true;
            });

            TestMacro = new RelayCommand(() =>
            {
                SendMidi(this.MacroChannel, 17);
            }, () => 
            {
                return this.IsMacroActivated && SelectedMidiDriver != null;
            });

            Task.Run(() => 
            {
                List<IMidiDriver> devices = new List<IMidiDriver>();
                int numberOfDevices = OutputDevice.DeviceCount;

                if (numberOfDevices > 0)
                {
                    for (int i = 0; i < numberOfDevices; i++)
                    {
                        devices.Add(new MidiDriver(i, OutputDevice.GetDeviceCapabilities(i)));
                    }
                }

                MidiDrivers = devices;
            });
        }

        private IMidiDriver _selectedMidiDriver_;

        public IMidiDriver SelectedMidiDriver
        {
            get { return _selectedMidiDriver_; }
            set 
            {
                Set("SelectedMidiDriver", ref _selectedMidiDriver_, value);

                (this.TestMacro as RelayCommand).RaiseCanExecuteChanged();
                (this.Start as RelayCommand).RaiseCanExecuteChanged();
            }        
        }

        private List<IMidiDriver> _midiDrivers;
        public List<IMidiDriver> MidiDrivers
        {
            get { return _midiDrivers; }
            private set { Set("MidiDrivers", ref _midiDrivers, value); }
        }

        static void Chk(int f)
        {
            if (0 == f) return;
            var sb = new StringBuilder(256); // MAXERRORLENGTH
            var s = 0 == midiOutGetErrorText(f, sb, sb.Capacity) ? sb.ToString() : String.Format("MIDI Error {0}.", f);
            System.Diagnostics.Trace.WriteLine(s);
        }

        public ICommand TestMacro { get; private set; }
        public ICommand Start { get; private set; }
        public ICommand Stop { get; private set; }
        public ICommand ClearAllData { get; private set; }
        public ICommand ImportExcel { get; private set; }
    }
}